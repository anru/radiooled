#include "HCuOLED.h"
#include "SPI.h"

#include <TEA5767.h>
#include <Wire.h>

/* Analog input pin for potentiometer */
#define POT_A1 1

/* Digital pins for down/up search buttons */
#define BTN2_DI 2
#define BTN3_DI 3

/* Digital pin number for the displays chip select pin */
#define CS_DI 10
/* Digital pin number for the displays data/command pin */
#define DC_DI 9
/* Digital pin number for the displays reset pin */
#define RST_DI 8

/* Last frequency set with potentiometer; -1.0 indicates fresh start */
float last_frequency = -1.0;
/* Search mode flag */
int search_mode = 0;
/* Search direction - up (1) or down (2); 0 indicates that no search is being done */
int search_direction = 0;
/* Timestamp for last performed search - call to process_search */
unsigned long last_searched = 0;
/* Buffer for radio flags, values */
unsigned char buf[5];

unsigned long current_millis;

HCuOLED OLED(SSD1307, CS_DI, DC_DI, RST_DI); // For SSD1307 displays (HCMODU0050 & HCMODU0052)

TEA5767 Radio;

void setup()
{
    Serial.begin(9600);
  
    Radio.init();
    Radio.set_frequency(95.8); // Radio Naba
  
    OLED.Reset();
}

void loop()
{
    int delay_val = 100;
    int flag_handle_button = 0;

    if (search_mode == 0) {
        int potentiometer_val = analogRead(POT_A1);
        for (int i = 0; i < 4; i++) {
            potentiometer_val += analogRead(POT_A1); 
            delay(1);
        }
        potentiometer_val = potentiometer_val / (1 + 4);
        
        /* map potentiometer value to FM frequencies */
        int frequencyInt = map(potentiometer_val, 2, 1022, 8700, 10800);
    
        float frequency = frequencyInt/100.0f;
        if (last_frequency == -1.0) {
            last_frequency = frequency;
        }
        else {
            if (frequency - last_frequency >= 0.1f || last_frequency - frequency >= 0.1f) {
                Radio.set_frequency(frequency);
                last_frequency = frequency;
            }
        }
 
        int buttonState = digitalRead(BTN2_DI);
        if (buttonState == HIGH) {
            search_direction = TEA5767_SEARCH_DIR_DOWN;
            flag_handle_button = 1;
        }
        buttonState = digitalRead(BTN3_DI);
        if (buttonState == HIGH) {
            search_direction = TEA5767_SEARCH_DIR_UP;
            flag_handle_button = 1;
        }
    }

    current_millis = millis();
    if (Radio.read_status(buf) == 1) {
        double current_freq = floor(Radio.frequency_available(buf) / 100000 + .5) / 10;
        int flag_stereo = Radio.stereo(buf);
        int signal_level = Radio.signal_level(buf);

        displayStats(current_freq, signal_level, flag_stereo);
    }

    if (current_millis - last_searched >= 500) {
        if (search_mode == 1
              && (search_direction == TEA5767_SEARCH_DIR_DOWN || search_direction == TEA5767_SEARCH_DIR_UP)) {

            int r = -1;
            if ((r = Radio.process_search(buf, search_direction)) == 1) {
                search_mode = 0;
                search_direction = 0;
            }

            last_searched = current_millis;
        }
        
    }

    if (flag_handle_button) {
        if (search_direction == TEA5767_SEARCH_DIR_DOWN) {
            Radio.search_down(buf);
            search_mode = 1;
            last_searched = 0;
        }
        else if (search_direction == TEA5767_SEARCH_DIR_UP) {
            Radio.search_up(buf);
            search_mode = 1;
            last_searched = 0;
        }
        flag_handle_button = 0;
        delay_val = 600;
    }

    delay(delay_val);
}

void displayStats(float frequency, int signal_level, int flag_stereo)
{
    char display_buf[8];

    OLED.ClearBuffer();
    OLED.DrawMode(NORMAL);

    OLED.SetFont(Terminal_8pt);
    OLED.Cursor(20, 10);
    snprintf_P(display_buf, sizeof(display_buf), PSTR("%s"), flag_stereo ? "Stereo" : "Mono");
    OLED.Print((char *) &display_buf[0]);

    OLED.Cursor(20, 56);
    snprintf_P(display_buf, sizeof(display_buf), PSTR("%d/15"), signal_level);
    OLED.Print((char *) &display_buf[0]);

    OLED.Cursor(20, 24);
    OLED.SetFont(LCDLarge_24pt);

    String frequency_str = String(frequency, 1);
    frequency_str.toCharArray(display_buf, 8);
    OLED.Print((char *) &display_buf[0]);

    OLED.Refresh();
}
